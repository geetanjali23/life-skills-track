# Caching

Caching is an easy way to improve performance of any system. It is faster to retrieve data from a cache than the original data source like databases. We generally keep cache at the front of the system so that we can retrieve data quickly. Caching helps us to use system resources efficiently and effectively. In this post, we'll learn all the aspects of caching. 

1. [What is Caching?](#what-is-caching)
1. [Why we need to use Caching?](#why-we-need-to-use-caching)
1. [Terms related to Caching?](#terms-related-to-caching)
1. [What is Cache Invalidation?](#what-is-cache-invalidation)
1. [Caching Techniques](#caching-techniques)
1. [Cache eviction policies](#cache-eviction-policies)
1. [References Section](#references-section)


---

## What is Caching?

It is a technique used for the performance improvement of a system.
Caching is storing data in a location different than the main data source such that it’s faster to access the data. It ensures low latency and high throughput. It has limited amount of space.

---

## Why we need to use Caching?

- Caching is done to avoid redoing the same complex computation again and again. It is used to improve the time complexity of algorithms


- Caching is used to speed up a system. To improve the latency of a system, we need to use caching. To reduce network requests can also be a cause for using caching.

- use caching to minimize data retrieval operation from the database

- It ensures low latency and high throughput. It has limited amount of space.

- It is a measure of poor design. We need to increase the number of hits and decrease the miss rate for performance improvement.

- Data can become stale if the primary source of data gets updated and the cache doesn’t

- If in a system, stale data is not a problem caching can quickly improve performance significantly

---

## Terms related to Caching?

1. When requested data is found in the cache, it’s called a Cache Hit. 

1. When the requested information is not found in the cache, it negatively affects a system. It’s called a Cache Miss

1. Client-Side Caching
Client level caching can be done so that client does not need to request to server-side

![caching](https://miro.medium.com/v2/resize:fit:828/format:webp/1*ZiFWhXlDzWbelMipKrS-2Q.jpeg)

1. Handle Database pressure:

we can cache the profile of the popular profile and get the data from there instead of risking losing the primary data source.

![caching handling pressure](https://miro.medium.com/v2/resize:fit:828/format:webp/1*WOgyH5nVA8iwtIHLLJGbaA.jpeg)

if the cache contains the previous data, then that is called stale data.

---

## What is Cache Invalidation?

we need to have a technique for invalidation of cache data. Else, the application will show inconsistent behavior. As cache has limited memory, we need to update data stored in it. This process is known as Cache Invalidation.


We can invalidate the cache data; also, we have to update the latest data in the cache. Otherwise, the system will search in the cache and not find the data and again go-to the database for data, affecting latency. Some techniques are used for cache data invalidation. 

---

## Caching Techniques

Techniques are used for caching invalidation

![different caching techniques](https://miro.medium.com/v2/resize:fit:828/format:webp/1*h7WKvgcsk_amdZ11G7mL2g.jpeg)


1. Write-through cache:
In this technique, data is written in the cache and DB. Before the data is written to the DB, the cache is updated with the data first.
 Cached data will provide fast retrieval so that performance will be faster. 
 this technique minimizes the data loss risk but write or update operation will have higher latency.

1. Write-back cache:
 we store data in the database in the other options, but here, data is written only to the cache.
 This technique is useful when the application is write-heavy. And it provides low latency for such applications. You can reduce the frequency of database writes with this strategy.
 this performance improvement comes with the risk of losing data in case of a crashed cache


1. Cache Aside:Cache Aside:
 the cache works along with the database trying to reduce the hits on DB as much as possible
 This approach works better for a read-heavy system; the data in the system is not frequently updated
 The problem with this approach is that the data present in the cache and the database could get inconsistent

1. Read-Through Cache
 similar to the Cache Aside strategy; The difference is that the cache always stays consistent with the database.
 A problem in this approach is that for the first time when information is requested by a user, it will be a cache miss

---

## Cache eviction policies:
The cache does not have a vast amount of space like a database. Also, in the case of stale data, we may need to remove them from the cache. So, cache eviction policies are important factors to consider while designing a cache.

1. First In First Out (FIFO)the cache behaves in the same way as a queue

1. Last In First Out (LIFO)The cache removes data that is most recently added

1. Least Frequently Used (LFU)we need to count how often a cache item is accessed.

1. Least Recently Used (LRU) the cache discards the least recently used data

1. Random Selection:randomly you might remove the data item which is most needed.

---

## References Section

- [System design medium article](https://towardsdatascience.com/system-design-basics-getting-started-with-caching-c2c3e934064a)

- [Learn Caching from basic](https://simplicable.com/IT/caching)

- [Caching mechanism gfg article](https://www.geeksforgeeks.org/what-is-the-caching-mechanism/)

- [blog on how caching works](https://auth0.com/blog/what-is-caching-and-how-it-works/)

- [Learn how to write Caching function in Python with CWH](https://www.youtube.com/watch?v=34p5CH48hLE)

---