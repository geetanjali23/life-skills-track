# Listening and Active Communication


### Question 1- What are the steps/strategies to do Active Listening?

- The act of fully hearing and comprehending the meaning of what someone else is saying.

- Focus of speaker and topic instead thinking of anything into the mind or seeing anyone around.

- Try not the interupt the other person let them finish and respond.

- Use door openers like tell me more, go ahead i'm listening, that's sound interesting etc. 

- Show the person you are listening with body language like do nod or say yes while listening to someone.

- Take notes of important conversation and ask the speaker if you didn't understand.



### Question 2 - According to Fisher's model, what are the key points of Reflective Listening? 

- Give focus on the conversation and try to ignore the distractions while listening

- Try to understand the feeling of the speaker. Listener should encourage the speaker to speak freely.

- Speaker through his words, posture and tone of voice shows their emotions and feelings. Listeners can understand the mood of the speaker and according to them they can react or give the answer.


### Question 3 - What are the obstacles in your listening process?

- Mental laziness: 
- Over Listening (Listening from long hours like youtube videos or attending online meetings)
- Surrounding Noises
- Visual distractions
- Physical Setting

### Question 4 - What can you do to improve your listening?

- See the speaker while listening
- Close the eyes and take deep breath
- Sit in the peaceful environment
- Give nod or say yes in between the conversation
- Take small breaks while doing work

### Question 5
When do you switch to Passive communication style in your day to day life?

- When somebody insulted me infront of group and i didn't feel comfortable but i didn't speak up for my self because of fear of society or group.


### Question 6
When do you switch into Aggressive communication styles in your day to day life?

- When somebody put the garbage infront of my house or have done the things which i said not to do something but they do the same or don't touch these items.


### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

- I say to my friends if i have been studied in good school or college today i would be very successful person in my life.


### Question 8
How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

- 



<!-- (passive communication not standing up for self. Stying quiet for our needs and feelings.
anxiety, depression, stress, burnout
aggressive communication standing up for self and not respecting the other person
passive aggressive: showing sarcasm or talking behind someone's back
assertive: standing up for self & respecting the other person

someone's base level of happiness we can't directly control
we're saying yes when our body language is saying no
communicate assertively is to communicate our actual needs and feelings as clearly and accurately as possible way in which they can easily understood state our feelings and needs as facts i prefer this i need this) -->