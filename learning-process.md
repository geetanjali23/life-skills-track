# Learning Process

### Question 1- What is the Feynman Technique? Paraphrase the video in your own words.

Feynman Technique says what you learn just explain this to someone in simple language or atleast pretend that you're explaining your learned concept to someone. This will increase your understanding in the learned concept.

The best way to test your understanding after learning new concept is that you should be able to break down complex concepts in simple english language so that other people could understand this. if you can't explain in your own words that means you didn't understand the concept you have to learn again

---


### Question 2- What are the different ways to implement this technique in your learning process?

We can apply Feynman Technique in our learning process in four steps. These are as follows-

1. Take a paper and write the concept's name at the top which you have learned.

1. Now, Start explaining or writing the concept in simple or plain english language. Also use examples to explain concept.

1. Most important step, Analyse or review your answer and identify problem areas. Go back to the sources to review and learn again.

1. Break down complicated terms which you used in your answer in simple language further.

### Question 3- Paraphrase the video in detail in your own words.

- The video is about how to learn more easily and effectively. She explained our brain do work or operation into two modes or ways-
1. Focus mode
1. Diffuse mode (resting state)

- Our brain is like pinball machine where balls go back and forth same in our brain information goes back and forth which is focus mode and diffuse mode.

- She also talk about what happens when our brain goes to procrastinate stage after diffuse mode. which tends to cram all things at the last moment.

- There are two ways to overcome from the procrastinate are-

1. Just keep working don't go to diffuse mode
1. Turn your attention away or temporary happiness like using social media or watching telivision etc

- The person who can't remember everything are more creative than others. If you are slow learner, obiously you have to work hard for grasping the things but you will understand in deep

- She gave some tips to improve the learning process.

---


### Question 4- What are some of the steps that you can take to improve your learning process?

---


- Meet different people who has same skill as your and take inputs from them of learning effectively

- If you are learning new concept and you're stuck not getting the concept. Take rest turn off your attention from the problem and give time to your brain to understanding the new concept

- Again if you are solving some problem and stuck. Take rest and do nothing just simply sit and think about the problem while doing this sometimes you can feel asleep which is called power nap. After sleep try to solve problem again with full energy and focus. Put your ideas from diffuse mode to focus mode

- But sometimes sit relax and come back can leads to procrastinate for this problem we can use Pomodoro Technique. Pomodoro Technique says work for 25mins with focus then take 5 mins break. Do this for 3 times continuously. Don't use mobile phones in between. This will increase your focus as well as you will also learn how to take rest between work.

- Use memory game to increase learning and remember ability.

- Take test of your self. Give timeline deadline to complete the work.

- Use flash cards to remember all important things. 

- Try to study different environment and situations.

- Learn songs lyrics to increase your memory and thinking ability.

- Use Recall technique. Read something and try to remember the things which you read on the page.

- Do practice of the leaning topics this will increase your understanding on the topic.

- Don't follow your passion just broden your passion. Means the things you like or the knowledge you have try to use this knowledge into another another field and mix your passion into different fields.

- Repeat the things which you learnt in a certain duration of period

---


### Question 5- Your key takeaways from the video? Paraphrase your understanding.

---


- The video is about if we want to learn new skill how much time this will take so according to some researchers we need 10000 hrs to become master in the particular skill

- To become top in any field we need lots of practice the more we will practice the better we will become in that field.

- If you are learning new skill for ourself we don't see time but if we want to learn something for others like for our company we have to learn the new skill in estipulated time.

- Do practice to become good at something very fast.

- If you put 20hrs in learning with full focus you will become very good in any new skill.

- Methods we have to use to learn some new skills in just 20hrs are-

1. Break the skill into smaller parts. Try to find the important parts of skills and practice these important parts. These important parts covers almost 65% of the new skill. In this way you can become good in any skill in least time.

1. Now after step 1 you have handsome knowledge of the new skill now you can start studying different resources for correcting your self.

1. Remove distractions while learning like mobile phone, television, talking to other. When you learning something just give your 100% attention.

1. Don't feel scare when you're learning new skill that how you will learn this new skill in just 20hrs just give your best and do practice for 20hrs. In this way you will remove your initial learning frustration barrier. 

---


### Question 6- What are some of the steps that you can while approaching a new topic?

---

- Use single material in the beginning. 
- Read the material and tell someone the concepts which you learned.
- Do practice and solve the problem of the topic.
- Use 20hrs strategy to learn new topic.
- Break down complex terminology into simple language.
- Recall the things which you learn and revise certain period of time.
- Now you have got some basic knowledge about the topic. You can start studying more resource to correct yourself.
- Don't feel scary at first if you're learning something new give rest to your brain and then come back to learn again. 
- Use pomodoro technique.
- Take test of yourself and analyse yourself and go back to again the resource and do study the resource


---
<!--
The key points which i have to improve is that whenever somebody will you and you're busy don't missed the call instead receive the call and inform that person that i am busy right now i will call you back after 10 mins.

If you have any doubts and you want to ask your mentor or senior then don't ask like this i have this problem will you help me instead say i am facing this problem and i did this to solve this problem attach the screenshots and share the code environment like codepen if possible and always be online when your mentor is giving you reply 



-->