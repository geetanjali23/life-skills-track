# Energy Management

## 1. Manage Energy not Time

### Question 1: What are the activities you do that make you relax - Calm quadrant?

---

- Talking with strangers.
- Taking rest or sleep.
- Doing Yoga.
- Eating something good.
- Reading and writing.
- Talking myself.
- Going to the temple.
- Listening to songs and watching movies.
- Doing hobby.
- Sit alone.
- Closing my eyes in the middle of work.

### Question 2: When do you find getting into the Stress quadrant?

---

- When i work hard but don't get the expected outcomes.
- Hating people for long period.
- Fear of reprimands.
- Tension of not being able to learn fast.
- Continuously working for long hours.
- Following the same routine.

### Question 3: How do you understand if you are in the Excitement quadrant?

---

- When i talk to different kinds of people over phone calls.
- To help others through my knowledge.
- Knowing about the world.
- Learning new skills.
- Working on my hobby makes me happy.

## 4. Sleep is your superpower

### Question 4 Paraphrase the Sleep is your Superpower video in detail.

---


Sleep is your Superpower video is by Sleep scientist Matt Walker.

- In this video, he explained people who don't take sleep 7 or 8 hours daily some bad changes start happening in their bodies because of lack of sleep. They won't be able to put much focus on working hours.

- People who take less than 6 hours of sleep daily start getting signs of aging, Alzheimer's disease, cancer cells start building in their body, heart attack cells growth increase, reproductive problems, weak immune system etc. They also live less than compared to people who take sleep well.

- He experimented on a group of people who took good sleep before night or who didn't. He found that the learning capacity in the sleep-well people group was good up to 40% as compared to people who took few hours of sleep.

- He told some ideas to improve sleep.
1. Don't take alcohol and caffeine before sleep.
2. Be Regular for bedtime and waking time.
3. The bedroom temperature should be cool.
4. Don't use the bed for other purposes like eating, reading etc. except sleep.
5. Give priority to your sleep.
6. Don't feel shame for taking 7 to 8 hours of sleep. This is nature's gift.
7. Give priority to your wellness.
8. For long life take good sleep.


### Question 5 What are some ideas that you can implement to sleep better?

---


- Do meditation before sleep.
- Sleep in the dark room.
- Sleep in a peaceful environment.
- Turn off mobile notifications.
- Do some warm yoga asanas.
- Listen to music.
- Start counting downwards from 1000 to 1.
- Talk to yourself e.g., how was your day?
- Focus on breathing.
- Remember the god.
- Smiling feeling happy that you are alive.
- Take dinner before sleep.

## 5. Brain-Changing Benefits of Exercise

### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

---
Brain-changing benefits of exercise by Wendy Suzuki

- Brain exercise means doing some physical activities like yoga, walking, going by stairs, jumping etc.
- It boosts energy, and focus, gives new ideas, increases decision-making capacity, and personality and maintains attention for long period.
- Exercise affects immediately the brain and starts increasing dopamine cells.
- Improves attention, increases the volume of the brain, make good mood, and protects the brain.
- Improves long-term memory, and decreases depression, and memory loss. Do 3 to 4 days a week for 30 mins.


### Question 7: What are some steps you can take to exercise more?

---


- Waking up around 6 a.m. before starting the daily work.
- Sleep early in the night around 10:30 pm.
- Do stretching while working.
- Do one minute of exercise. Close your eyes, sit straight, and give focus to your breath for one minute.
- Follow routine.
- Do physical work and avoid using machines for small tasks like washing clothes or lifts etc.

---