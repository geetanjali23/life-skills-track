## Question 1 Paraphrase (summarize) the video in a few lines. Use your own words.

The video is about Grit and Growth mindset.
- Grit means how much you have the passion and perseverance to work for your goal. It is about working every day for your goal instead of few days. If you will work hard everyday for your goal without scaring from the tasks or thinking about the results then only you'll be able to achieve your goal.

- If you are talented but don't have grit quality that means you don't work daily for your goal then your talent won't help you also to achieve your goal.

- We can build grit in ourselves is by growth mindset means we have to accept our failure because failure isn't permanent state of life. We should take lessons from our failure and start working towards our goal with full potential again. We shouldn't think that we can't do this. We always be persevere towards our goal.

- The ability to learn isn't fixed, by putting much more efforts and extra time we can learn and grow fast.

## Question 2 What are your key takeaways from the video to take action on?

- Divide your big goal into small small tasks and should work daily to achieve your goal.

- Don't scare from your failure just take lessons from them and starting working again.

- You have unlimited capacity to learn new things. By putting extra time we can become good at any field.

## Question 3 Paraphrase (summarize) the video in a few lines in your own words.

The video is about Growth Mindset.

- This video is all about Improving and changing the way of learning. The belief in your capacity to learn and grow.

- He explained Why some people succeed while people who are equally talented don't. People mindset plays a crucial role to achieve success.

- There are two types of mindset that is Fixed learning mindset and growth learning mindset.

- Fixed mindset people think that the people who are good at something they are talented by birth while growth mindset people thinks that the things can be learn and believe that the people who are good in something they have build their ability on the skill by doing hard work.

- People with fixed mind set thinks that they can't learn and grow while the growth mindset people think you can learn the things and grow.

- Growth mindset means be positive don't think negative that you can't learn this and they also don't make excuses and don't point out on their ability. 

- The growth mindset is that you can get better it doesn't matter what you did in past, how many achievements you have, what is your age, what you have done in your carreer. It's just about growth mindset to believe that you can grow and learn.

- People in a fixed mindset tend to focus on performance, outcomes, results and they care about people what people will say about them. Their main focus what they will get, how they look rather than to not look bad.

- Four key ingredients to growth(BELIEF, FOCUS RELATED TO THESE FOUR INGREDIENTS )
1. Efforts
2. Challenges
3. Mistakes
4. Feedback

- The people who have fixed mind set they see the things bad like they think there is no point to learn the thing in which we're not good, it is useless to put the effort in the not knowing thing.

- The fixed mindset people think to avoid challenges, threat and pain, get discouraged when they do mistakes. They get defensive, they take personally to the feedback. They avoid to work on challenges or taks because they don't want to look bad.They don't believe that they can change. They don't believe in their capacity to grow. This kind of people don't like to do these four key ingredients of growth.

- The growth mindset people see efforts as a part of learning, they embrace the challenges and become persevere to complete the goals. They see mistakes or challenges as a learning opportunity. They accept the feedback and learn it and incorporate them in their lifes.

- They believe that the feedback or suggested tips and information can grow them and can make them good in the skill. They accept that in different days or time you can be grow or can be stagnant, everyday isn't similar.

## Question 4: What are your key takeaways from the video to take action on?

- Believe in myself that i can do it.
- Focus on how i'm learning or approaching the things.
- Make efforts to complete the goals on a daily basis.
- Don't be scare from the challenges.
- Learn from mistakes and improve it. Try to not repeat the same mistake again.
- Take feedback positively and incorporate in the life or work.

## Question 5 What is the Internal Locus of Control? What is the key point in the video?
 

 - This video is about Increase in levels of motivation.

 - To feel motivated all the time to do any kind of work in any situation. Give accomplishment yourself when you solve some problems. The belief you have control on your life. This is internal locus of control. To solve the problems you won't need motivation.

 - Eternal or external locus of control means you can't control you're born smart or not(smart kids). Internal locus of control, you have control on hard work and this is the key to stay motivated.
## Question 6 Paraphrase (summarize) the video in a few lines in your own words.

- The video is about "How to Develop a Growth Mindset".

- Believe in your ability to figure things out and you can get better. This also makes you motivated towards doing the work.

- The person who don't believe in them self they don't have any expectation from their self that they can do this thing and can become good at somethng.

- Don't share your thoughts(introvert) with other questioning on yourself that who am i to say this stop negative thoughts which questions on yourself that you can't do this.

- Decide your life path to achieve your goals or what you want to learn. Open mindset and follow the curriculum which you decide the learn the new things.

- Discouragement honor the struggle don't hate the process. This is the part of learning journey.

- The growth mindset people accept the new challenges and plan the way how they're going to complete this and follow the curriculum and done with the new challenges. They believe that this challenge will grow as an individual.
## Question 7 What are your key takeaways from the video to take action on?

- Believe in yourself.
- Don't be scare from challenges.
- Share your thoughts to the world.
- Decide your goals, make a plan and commit to the plan.
- Accept the struggle of learning any thing new.
## Question 8 What are one or more points that you want to take action on from the manual? (Maximum 3)

1. I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.

2. I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.

3. I am very good friends with Confusion, Discomfort and Errors.
- Confusion tells me there is more understanding to be achieved.
- Discomfort tells me i have to make an effort to understand. I understand the learning process is uncomfortable.
- Errors tell me what is not working in my code and how to fix it.

4. I will not leave my code unfinished till i complete the following checklist:
- Make it work.
- Make it readable.
- Make it modular.
- make it efficient.

5. I will follow the steps required to solve problems:
- Relax.
- Focus- What is the problem? What do i need to know to solve it?
- Understand - Documentation, Google, Stack Overflow, Github Issues, Internet.
- Code.
- Repeat.

 ---