# Prevention of Sexual Harassment

### Question 1: What kinds of behavior cause sexual harassment?

---

- There are three forms of Sexual Harassment behavior- Verbal, visual and physical.

- Verbal harassment behavior includes comments about clothing, body, threat, rumors about a person's sexual or personal life, and using obscene language.

- Visual harassment includes postures, drawings, screensavers, cartoons, emails, and text of a sexual nature.

- Physical harassment includes impeding or blocking movement, and inappropriate touching such as kissing hugging, patting, stroking, rubbing, sexual gesture leering or staring.

- There are two categories of sexual harassment- Quid pro Quo and hostile work environment.

- In Quid pro Quo (This for that), related to offering good things like promotions, increments in salary etc. for sexual relationships.

- A hostile work environment means when an employee repeatedly passes sexual comments or jokes and makes another employee uncomfortable by praising every time.

- Any unwelcome verbal, visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment.

- Asking again and again for dates or going out if already person mentioned no for that.

- Giving indirect signals for sexual relationships.

- Inappropriate conduct or comment by a person towards your worker and humiliation or intimidation.

- Yelling or calling names inappropriately.

- Giving personal comments, harmful hazing practices or different kinds of perpetrators.
 
- Bullying can be between coworkers and employers and can come from the public.

- Sending emails, text messages and social websites which contain sexual harassment. This kind of behavior comes under cyberbullying.


### Question 2: What would you do in case you face or witness any incident or repeated incidents of such behavior?

---

- Will take reasonable steps to prevent bullying and harassment or minimize it where possible.

- Legal obligations for employers, supervisors and workers

- We should give written complaints about these incidents to our supervisors.

- Should be aware of Companies' policies toward harassment and procedure for reporting incidents of harassment. So that company can take immediate positive actions.

- Give training to supervisors and workers so that they can find which kind of behavior can come under in sexual harassment and they can start improving their behavior

- Should tell Supervisors and workers about these incidents.

- To tell the person to stop the harassment.

- Talk to your supervisor.

- Awareness of sexual harassment.

- I will try to understand the incident. Because this can also be my perception means the behaviour which is uncomfortable for me for others it can't.

---


